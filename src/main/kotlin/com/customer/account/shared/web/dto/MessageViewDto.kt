package com.customer.account.shared.web.dto


data class MessageViewDto(val code: String = "", var message: String = "")

