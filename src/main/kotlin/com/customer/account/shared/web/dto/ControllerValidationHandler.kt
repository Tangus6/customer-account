package com.customer.account.shared.web.dto


import com.customer.account.shared.exceptions.GlobalBusinessException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus

@ControllerAdvice
class ControllerValidationHandler() {

    @ExceptionHandler(GlobalBusinessException::class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun processEntityNotFound(ex: GlobalBusinessException): MessageViewDto {
        return MessageViewDto(
                code = ex.code,
                message = ex.messageArg
        )
    }
}
