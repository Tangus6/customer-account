package com.customer.account.shared.exceptions

class GlobalBusinessException(val messageArg: String, val code: String) : RuntimeException() {

}