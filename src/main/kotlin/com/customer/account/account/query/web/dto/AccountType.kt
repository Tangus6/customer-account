package com.customer.account.account.query.web.dto

enum class AccountType(val desc: String) {
    SAVING("saving"),
    STOCK("stock");
}