package com.customer.account.account.query.persistance.document

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.math.BigDecimal
import java.util.*


@Document(collection = "account")
class AccountDocument(
        @Field(name = "account_name")
        var accountType: String,
        @Field(name = "current_balance")
        var currentBalance: BigDecimal,
        @Id
        var id: UUID = UUID.randomUUID()
) {
    override fun toString(): String {
        return "AccountDocument(accountType='$accountType', currentBalance=$currentBalance, id=$id)"
    }
}
