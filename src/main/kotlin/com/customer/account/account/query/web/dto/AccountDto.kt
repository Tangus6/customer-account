package com.customer.account.account.query.web.dto

data class AccountDto(
        val currentBalance: String = "",
        val accountType: String = ""
)