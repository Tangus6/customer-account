package com.customer.account.account.query


import com.customer.account.account.query.web.dto.AccountDto
import com.customer.account.currency.CurrencyExchanger
import com.customer.account.customer.peristance.repository.CustomerRepository
import com.customer.account.shared.exceptions.GlobalBusinessException
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*
import java.util.Locale


class AccountQueryFacade(private val customerRepository: CustomerRepository,
                         private val currencyExchanger: CurrencyExchanger) {

    private val log = LoggerFactory.getLogger(this.javaClass.name)

    fun getCustomerAccountBalance(customerId: String, accountType: String): AccountDto {

        val customerOptional = customerRepository.findById(UUID.fromString(customerId))

        return if (customerOptional.isEmpty) {
            log.info("There was no customer with id: $customerId")
            throw  GlobalBusinessException(code = "s02", messageArg = "There was no customer with provided id")
        } else {
            val customer = customerOptional.get()
            val account = customer.accounts.find { accountDocument -> accountDocument.accountType == accountType }
            if (account != null) {
                val currentBalanceUsd: BigDecimal = currencyExchanger.convertPlnToUsd(account.currentBalance)

                val formatter: NumberFormat = NumberFormat.getCurrencyInstance(Locale.US)
                val currentBalanceUsdFormat: String = formatter.format(currentBalanceUsd)

                AccountDto(currentBalance = currentBalanceUsdFormat, accountType = account.accountType)
            } else {
                log.info("Customer with id $customerId have zero accounts")
                throw  GlobalBusinessException(code = "s02", messageArg = "Customer with provided id have zero accounts")
            }
        }
    }
}



