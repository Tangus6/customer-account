package com.customer.account.account.query.web


import com.customer.account.account.query.AccountQueryFacade
import com.customer.account.account.query.web.dto.AccountDto
import com.customer.account.configuration.UrlMapping
import com.customer.account.configuration.UrlMapping.AccountUrlMapping
import com.customer.account.configuration.UrlMapping.AccountUrlMapping.ACCOUNT_TYPE_ID_PARAM_NAME
import com.customer.account.customer.peristance.document.CustomerDocument
import com.customer.account.customer.peristance.repository.CustomerRepository
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
@RestController
internal class AccountQueryController(private val accountQueryFacade: AccountQueryFacade, private val customerRepository: CustomerRepository) {

    @GetMapping(value = [AccountUrlMapping.ACCOUNT_TYPE])
    fun getCustomerAccountBalance(@PathVariable("customer_id") customerId: String,
                                  @PathVariable(ACCOUNT_TYPE_ID_PARAM_NAME) accountId: String): ResponseEntity<AccountDto> {

        return ResponseEntity.status(HttpStatus.OK)
                .body(accountQueryFacade.getCustomerAccountBalance(customerId, accountId))
    }

    @GetMapping(value = [UrlMapping.CustomerUrlMapping.BASE])
    fun getCustomerAccountBalance(): ResponseEntity<MutableList<CustomerDocument>> {

        return ResponseEntity.status(HttpStatus.OK)
                .body(customerRepository.findAll())
    }
}
