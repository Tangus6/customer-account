package com.customer.account.pupulate


import com.customer.account.account.query.persistance.document.AccountDocument
import com.customer.account.account.query.web.dto.AccountType.SAVING
import com.customer.account.account.query.web.dto.AccountType.STOCK
import com.customer.account.configuration.AppConstant.Profile.TEST_PROFILE
import com.customer.account.customer.peristance.document.CustomerDocument
import com.customer.account.customer.peristance.repository.CustomerRepository
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.stereotype.Component
import java.math.BigDecimal
import javax.annotation.PostConstruct


@Component
@Profile(value = ["!$TEST_PROFILE"])
internal class InitialDB(
        private val mongoTemplate: MongoTemplate,
        private val customerRepository: CustomerRepository
) {
    private val log = LoggerFactory.getLogger(this.javaClass.name)

    @PostConstruct
    fun initial() {
        mongoTemplate.db.drop();
        for (i in 1..10) {
            val accountsDocuments = listOf(AccountDocument(accountType = SAVING.desc, currentBalance = BigDecimal.valueOf(100)),
                    AccountDocument(accountType = STOCK.desc, currentBalance = BigDecimal.valueOf(40000)))

            val customerDocument = CustomerDocument(firstName = "Stefan", lastName = "Burczynski", email = "stefan.bur$i@gmail.com",
                    accounts = accountsDocuments);
            val customerDocPersisted = customerRepository.save(customerDocument)
            log.info("------------------data persisted start")
            log.info(customerDocPersisted.toString())
            log.info("------------------data persisted end")
        }


    }
}





