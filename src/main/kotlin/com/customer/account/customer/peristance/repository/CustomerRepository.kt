package com.customer.account.customer.peristance.repository

import com.customer.account.customer.peristance.document.CustomerDocument
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.util.*


@Repository
interface CustomerRepository : MongoRepository<CustomerDocument, UUID> {
}