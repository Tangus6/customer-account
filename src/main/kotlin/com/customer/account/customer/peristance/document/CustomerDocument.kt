package com.customer.account.customer.peristance.document

import com.customer.account.account.query.persistance.document.AccountDocument
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.util.*


@Document(collection = "customer")
class CustomerDocument(
        @Field(name = "first_name")
        var firstName: String,
        @Field(name = "last_name")
        var lastName: String,
        @Field(name = "email")
        var email: String,
        var accounts: List<AccountDocument> = emptyList(),
        @Id
        var id: UUID? = null
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as CustomerDocument

        if (id == other.id) return true

        if (firstName == other.firstName && lastName == other.lastName
                && email == other.email && accounts == other.accounts) return true


        return false
    }

    override fun hashCode(): Int {
        var result = firstName.hashCode()
        result = 31 * result + lastName.hashCode()
        result = 31 * result + email.hashCode()
        result = 31 * result + accounts.hashCode()
        result = 31 * result + (id?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "CustomerDocument(firstName='$firstName', lastName='$lastName', email='$email', accounts=$accounts, id=$id)"
    }
}
