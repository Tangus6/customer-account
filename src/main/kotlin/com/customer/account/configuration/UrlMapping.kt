package com.customer.account.configuration


object UrlMapping {
    private const val API_BASE_PATH = "/api"

    object CustomerUrlMapping {
        const val BASE = "$API_BASE_PATH/customers"
    }

    object AccountUrlMapping {
        private const val BASE = "${CustomerUrlMapping.BASE}/{customer_id}/accounts"
        const val ACCOUNT_TYPE_ID_PARAM_NAME = "account_type"
        const val ACCOUNT_TYPE = "${BASE}/{$ACCOUNT_TYPE_ID_PARAM_NAME}"
    }
}