package com.customer.account.configuration.db

import com.customer.account.customer.peristance.document.CustomerDocument
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent
import org.springframework.stereotype.Component
import java.util.*


@Component
internal class GenerateUUIDListener : AbstractMongoEventListener<CustomerDocument>() {
    override fun onBeforeConvert(event: BeforeConvertEvent<CustomerDocument>) {
        val customer: CustomerDocument = event.source
        if (Objects.isNull(customer.id)) {
            customer.id = UUID.randomUUID()
        }
    }
}