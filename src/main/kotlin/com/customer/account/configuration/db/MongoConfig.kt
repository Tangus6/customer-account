package com.customer.account.configuration.db


import com.customer.account.customer.peristance.repository.CustomerRepository
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories


@Configuration
@EnableMongoRepositories(basePackageClasses = [CustomerRepository::class])
internal class MongoConfig {

}