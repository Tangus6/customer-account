package com.customer.account.configuration.app


import com.customer.account.account.query.AccountQueryFacade
import com.customer.account.currency.CurrencyExchanger
import com.customer.account.customer.peristance.repository.CustomerRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class Beans {

    @Bean
    fun accountQueryFacade(customerRepository: CustomerRepository,
                           currencyExchanger: CurrencyExchanger): AccountQueryFacade {
        return AccountQueryFacade(
                customerRepository = customerRepository, currencyExchanger = currencyExchanger)
    }
}