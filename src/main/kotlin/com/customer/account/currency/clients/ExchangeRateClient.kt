package com.customer.account.currency.clients

import com.customer.account.currency.dto.ExchangeDto
import com.customer.account.shared.exceptions.GlobalBusinessException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import java.math.BigDecimal

@Component
class ExchangeRateClient() {
    @Autowired
    private lateinit var restTemplate: RestTemplate

    companion object {
        const val URL_USD_RATE: String = "http://api.nbp.pl/api/exchangerates/rates/A/USD/today/"
    }

    private val log = LoggerFactory.getLogger(this.javaClass.name)

    fun getExchangeRate(): BigDecimal {
        val httpHeaders = HttpHeaders()
        httpHeaders.contentType = MediaType.APPLICATION_JSON
        val responseEntity: ResponseEntity<ExchangeDto>? = restTemplate.getForEntity(URL_USD_RATE, ExchangeDto::class.java, httpHeaders)

        return if (responseEntity != null && responseEntity.statusCode == HttpStatus.OK && responseEntity.body != null) (
                return responseEntity.body!!.rates[0].mid
                ) else {
            log.error("Could not get exchange rate")

            throw GlobalBusinessException(code = "s05", messageArg = "Could not get exchange rate")
        }
    }
}