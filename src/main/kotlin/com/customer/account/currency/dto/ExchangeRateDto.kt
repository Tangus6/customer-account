package com.customer.account.currency.dto

import java.math.BigDecimal

class ExchangeRateDto(
        var no: String = "",
        var effectiveDate: String = "",
        var mid: BigDecimal = BigDecimal.ZERO
)