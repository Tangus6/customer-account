package com.customer.account.currency.dto

class ExchangeDto(
        var table: String = "",
        var currency: String = "",
        var code: String = "",
        var rates: List<ExchangeRateDto> = emptyList()
)