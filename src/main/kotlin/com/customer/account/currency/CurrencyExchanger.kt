package com.customer.account.currency

import com.customer.account.currency.clients.ExchangeRateClient
import org.springframework.stereotype.Component
import java.math.BigDecimal

@Component
class CurrencyExchanger(private val exchangeRateClient: ExchangeRateClient) {

    fun convertPlnToUsd(currentBalance: BigDecimal): BigDecimal {
        val exchangeRate = exchangeRateClient.getExchangeRate()
        return exchangeRate.multiply(currentBalance)
    }
}


