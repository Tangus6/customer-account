package com.customer.account

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CustomerAccountApplication

fun main(args: Array<String>) {
	runApplication<CustomerAccountApplication>(*args)
}
