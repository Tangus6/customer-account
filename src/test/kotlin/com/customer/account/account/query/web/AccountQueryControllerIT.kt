package com.customer.account.account.query.web

import com.customer.account.account.query.persistance.document.AccountDocument
import com.customer.account.account.query.web.dto.AccountDto
import com.customer.account.account.query.web.dto.AccountType
import com.customer.account.configuration.AppConstant.Profile.TEST_PROFILE
import com.customer.account.configuration.UrlMapping.AccountUrlMapping
import com.customer.account.currency.clients.ExchangeRateClient.Companion.URL_USD_RATE
import com.customer.account.currency.dto.ExchangeDto
import com.customer.account.currency.dto.ExchangeRateDto
import com.customer.account.customer.peristance.document.CustomerDocument
import com.customer.account.customer.peristance.repository.CustomerRepository
import com.customer.account.shared.web.dto.MessageViewDto
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.test.context.ActiveProfiles
import org.springframework.web.client.RestTemplate
import java.math.BigDecimal


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = [TEST_PROFILE])
class AccountQueryControllerIT {

    @Autowired
    private lateinit var testRestTemplate: TestRestTemplate

    @Autowired
    private lateinit var restTemplateMock: RestTemplate

    @Autowired
    private lateinit var customerRepository: CustomerRepository

    @Test
    fun getCustomerAccountBalance() {
        //given
        val rateUSD = BigDecimal.valueOf(3.13)
        val exchangeDto = ExchangeDto(rates = listOf(ExchangeRateDto(mid = rateUSD)))
        val httpHeaders = HttpHeaders()
        httpHeaders.contentType = MediaType.APPLICATION_JSON
        Mockito.`when`(restTemplateMock.getForEntity(URL_USD_RATE, ExchangeDto::class.java, httpHeaders)).thenReturn(ResponseEntity.ok(exchangeDto))

        val currentBalance = BigDecimal.valueOf(100)
        val accountType = AccountType.SAVING.desc
        val customerDocument = CustomerDocument(firstName = "Stefan", lastName = "Burczynski", email = "stefan.bur@gmail.com",
                accounts = listOf(AccountDocument(accountType = accountType, currentBalance = currentBalance)))

        val customerDocPersisted = customerRepository.save(customerDocument)
        val urlCustomerBalance = AccountUrlMapping.ACCOUNT_TYPE
                .replace("{customer_id}", customerDocPersisted.id.toString())
                .replace("{account_type}", accountType)

        // when
        val responseEntity: ResponseEntity<AccountDto> = testRestTemplate.getForEntity(
                urlCustomerBalance,
                object : ParameterizedTypeReference<ResponseEntity<AccountDto>>() {}.type
        )

        //then
        assertThat(responseEntity.statusCode)
                .isEqualTo(HttpStatus.OK)

        val accountDto: AccountDto = responseEntity.body as AccountDto

        assertThat(accountDto)
                .hasFieldOrPropertyWithValue(AccountDto::accountType.name, accountType)
                .hasFieldOrPropertyWithValue(AccountDto::currentBalance.name, "$313.00")

    }

    @Test
    fun getCustomerAccountBalance_when_exchange_return_null() {
        //given
        val httpHeaders = HttpHeaders()
        httpHeaders.contentType = MediaType.APPLICATION_JSON
        Mockito.`when`(restTemplateMock.getForEntity(URL_USD_RATE, ExchangeDto::class.java, httpHeaders)).thenReturn(null)

        val currentBalance = BigDecimal.valueOf(100)
        val accountType = AccountType.SAVING.desc
        val customerDocument = CustomerDocument(firstName = "Stefan", lastName = "Burczynski", email = "stefan.bur@gmail.com",
                accounts = listOf(AccountDocument(accountType = accountType, currentBalance = currentBalance)))

        val customerDocPersisted = customerRepository.save(customerDocument)
        val urlCustomerBalance = AccountUrlMapping.ACCOUNT_TYPE
                .replace("{customer_id}", customerDocPersisted.id.toString())
                .replace("{account_type}", accountType)

        // when
        val responseEntity: ResponseEntity<MessageViewDto> = testRestTemplate.getForEntity(
                urlCustomerBalance,
                object : ParameterizedTypeReference<ResponseEntity<MessageViewDto>>() {}.type
        )

        //then
        assertThat(responseEntity.statusCode)
                .isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR)

        val messageViewDto: MessageViewDto = responseEntity.body as MessageViewDto

        assertThat(messageViewDto)
                .hasFieldOrPropertyWithValue(MessageViewDto::code.name, "s05")
                .hasFieldOrPropertyWithValue(MessageViewDto::message.name, "Could not get exchange rate")

    }

    @TestConfiguration
    internal class AccountQueryControllerConfig {
        @Bean
        @Primary
        fun restTemplateMock(): RestTemplate {
            return Mockito.mock(RestTemplate::class.java)
        }
    }


}